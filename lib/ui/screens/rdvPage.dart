import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:select_form_field/select_form_field.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'listSpecialite.dart';
import 'specialite.dart';

void main()=>runApp(MonApp());
class MonApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}

class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _Home();
    throw UnimplementedError();
  }


}

class _Home extends State<Home>{

  List a= List();
  Future getAllDoctor() async {
    var url = "http://192.168.43.115/app_mob_localhost/DropDoctor.php";
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body);
      setState(() {
        a = jsonData;
      });
    }
  }

  final List<Map<String, dynamic>> medecin = [
    {
      'value': 'leonel65',
      'label': 'leonel65',
    },
    {
      'value': 'gaby',
      'label': 'gaby',
    },
    {
      'value': 'ange',
      'label': 'ange',
    },
  ];
  bool editable = true;
  DateTime date;  InputType inputType = InputType.date;
  InputType inputTypes = InputType.time;
  final formats = {
    InputType.both: DateFormat("EEEE, MMMM d, yyyy 'at' h:mma"),
    InputType.date: DateFormat('yyyy-MM-dd'),
    InputType.time: DateFormat("HH:mm"),
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body:Padding(
          padding: EdgeInsets.only(left: 25.0,right: 25.0,bottom: 25.0),
          child: ListView(
            children: <Widget>[
              Text(
                'Prendre un rendez-vous',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontFamily: 'Satisfy',
                    color: Colors.pink[400],
                    fontSize: 26.0,
                    letterSpacing: 1.0
                ),
              ),
              Padding(
                padding:EdgeInsets.only(bottom: 25.0),
              ),
              SelectFormField(
                labelText: 'Specialité',
                items:medecin,
                onChanged: (val) => print(val),
                onSaved: (val) => print(val),
              ),
              Padding(
                padding:EdgeInsets.only(bottom: 10.0),
              ),
              SelectFormField(
                labelText: 'Medecin',
                items: medecin,
                onChanged: (val) => print(val),
                onSaved: (val) => print(val),
              ),
              Padding(
                padding:EdgeInsets.only(bottom: 10.0),
              ),
              DateTimePickerFormField(
                inputType: inputType,
                format: formats[inputType],
                editable: editable,
                decoration: InputDecoration(
                  labelText: 'Date rdv',
                  fillColor: Colors.white,
                ),

              ),
              Padding(
                padding:EdgeInsets.only(bottom: 10.0),
              ),
              DateTimePickerFormField(
                inputType: inputTypes,
                format: formats[inputTypes],
                editable: editable,
                decoration: InputDecoration(
                  labelText: 'heure du rdv',
                  fillColor: Colors.white,
                ),
              ),
              Padding(
                padding:EdgeInsets.only(bottom: 15.0),
              ),
              TextField(
                maxLines: 8,
                keyboardType: TextInputType.multiline,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'motif',
                ),
              ),
              Padding(
                  child:RaisedButton(
                    color: Colors.pink[400],
                    onPressed: () {},
                    child: Text(
                      "envoyer",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'Satisfy',
                        fontSize: 20,
                      ),
                    ),
                  ) ,
                  padding:EdgeInsets.only(top:10.0,left: 185.0)
              )
            ],
          ),
        )
    );
  }
}