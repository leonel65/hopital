import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';
import 'rdv.dart';

const users = const {
  'doctor@gmail.com': 'leonel65',
};

class LoginAdmin extends StatelessWidget {
  Duration get loginTime => Duration(milliseconds: 2250);

  Future<String> _authUser(LoginData data) {
    print('Name: ${data.name}, Password: ${data.password}');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(data.name)) {
        return 'Username not exists';
      }
      if (users[data.name] != data.password) {
        return 'Password does not match';
      }
      return null;
    });
  }

  Future<String> _recoverPassword(String name) {
    print('Name: $name');
    return Future.delayed(loginTime).then((_) {
      if (!users.containsKey(name)) {
        return 'Username not exists';
      }
      return null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FlutterLogin(
      theme: LoginTheme(accentColor: Colors.white,titleStyle: TextStyle(fontSize: 32,fontFamily: 'Texturina',color: Colors.white),primaryColor:Colors.blue),
      title: 'Login Medecin',
      logo: 'assets/images/icon.png',
      onLogin: _authUser,
      onSignup: _authUser,
      onSubmitAnimationCompleted: () {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => Dash(),
        ));
      },

    );
  }
}