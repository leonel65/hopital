import 'package:flutter/material.dart';
import 'package:chu/ui/screens/page2.dart';

void main() => runApp(Rd());

class Rd extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return MaterialApp
      (
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primarySwatch: Colors.blue),
      home: MainPage(),
    );
  }
}