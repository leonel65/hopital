import 'dart:convert';

import 'package:chu/ui/screens/specialite.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

Future<List<Specialite>> fetchPhotos(http.Client client) async {
  final response =
  await client.get('https://jsonplaceholder.typicode.com/photos');

  // Use the compute function to run parsePhotos in a separate isolate.
  return compute(parseSpecialite, response.body);
}

List<Specialite> parseSpecialite(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();

  return parsed.map<Specialite>((json) => Specialite.fromJson(json)).toList();
}