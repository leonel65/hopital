import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:chu/ui/screens/page2.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;


void main() => runApp(new Contact());

class Contact extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: new ThemeData(primaryColor: Color.fromRGBO(58, 66, 86, 1.0)),
      home: new ListPage(title: 'Contact'),
    );
  }
}
class ListPage extends StatefulWidget {
  ListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {

  List user = [];
  bool isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.fetchUser();
  }

  fetchUser() async {
    setState(() {
      isLoading = true;
    });
    var url = "http://192.168.43.154/rest/api/post/doctor.php";
    var response = await http.get(url);
    // print(response.body);
    if (response.statusCode == 200) {
      var items = json.decode(response.body);
      setState(() {
        user = items;
        isLoading = false;
      });
    } else {
      user = [];
      isLoading = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    List<String> doctorname = [];
    List<String> departmentname = [];
    List<String> mobilno = [];

    for(int i=0;i<user.length;i++){
      doctorname.add(user[i]['doctorname']);
      departmentname.add(user[i]['departmentname']);
      mobilno.add(user[i]['mobileno']);
    }
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back,color: Colors.white,),
          onPressed:() => Navigator.of(context).push(MaterialPageRoute(builder: (_) => MainPage())),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue,
        title: Text('Contact', style: TextStyle(color: Colors.black, fontSize: 25.0,fontFamily: 'Texturina')),

      ),
      body: Container(
        child: Padding(
          padding: EdgeInsets.only(top: 20),
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: user.length,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                elevation: 8.0,
                margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                child: Container(
                  decoration: BoxDecoration(color: Colors.white12),
                  child: ListTile(
                    contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    leading: Container(
                      padding: EdgeInsets.only(right: 12.0),
                      decoration: new BoxDecoration(
                          border: new Border(
                              right: new BorderSide(width: 1.0, color: Colors.black26))),
                      child: Image(image: AssetImage('assets/images/doctor2.png'),),
                    ),
                    trailing:Wrap(
                      children: <Widget>[
                        IconButton(icon: Icon(Icons.mail_rounded,color: Colors.redAccent,), onPressed: () => launch("tel://698083345")), // icon-1
                        IconButton(icon: Icon(Icons.phone,color:Colors.blue), onPressed: () => launch("tel://"+mobilno[index])),
                      ],
                    ),
                    title: Text(
                      doctorname[index],
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Texturina',
                          fontSize: 17.0
                      ),
                    ),
                    subtitle: Text(
                        departmentname[index],
                      style: TextStyle(
                        fontFamily: 'Texturina',
                        color: Colors.black38
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),

    );
  }
}