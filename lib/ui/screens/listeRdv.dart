import 'package:flutter/material.dart';
import 'package:chu/ui/screens/page2.dart';
void main() => runApp(new ListeRdv());

class ListeRdv extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: new ThemeData(primaryColor: Color.fromRGBO(58, 66, 86, 1.0)),
      home: new ListPage(title: 'dossier medical'),
    );
  }
}
class ListPage extends StatefulWidget {
  ListPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {

  final makeCard = Card(
    elevation: 8.0,
    margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
    child: Container(
      decoration: BoxDecoration(color: Colors.white12),
      child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: new BoxDecoration(
                border: new Border(
                    right: new BorderSide(width: 1.0, color: Colors.black26))),
            child: Icon(Icons.calendar_today_rounded, color: Colors.black26),
          ),
          title: Text(
            "Consultation medical",
            style: TextStyle(
                color: Colors.black38,
                fontWeight: FontWeight.bold,
                fontFamily: 'Texturina',
              fontSize: 17.0
            ),
          ),
          // subtitle: Text("Intermediate", style: TextStyle(color: Colors.white)),

          subtitle: Row(
            children: <Widget>[
              Icon(Icons.linear_scale, color: Colors.pink[400]),
              Text(" Intermediate", style: TextStyle(color: Colors.white))
            ],
          ),
      ),
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back,color: Colors.white,),
          onPressed:() => Navigator.of(context).push(MaterialPageRoute(builder: (_) => MainPage())),
        ),
        centerTitle: true,
        backgroundColor: Colors.blue,
        title: Text('Dossier médical', style: TextStyle(color: Colors.black, fontSize: 25.0,fontFamily: 'Texturina')),

      ),
      body: Container(
        child: Padding(
          padding: EdgeInsets.only(top: 20),
          child: ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: 4,
            itemBuilder: (BuildContext context, int index) {
              return makeCard;
            },
          ),
        ),
      ),

    );
  }
}