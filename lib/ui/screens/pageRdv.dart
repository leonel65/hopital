import 'package:flutter/material.dart';
import 'package:validators/validators.dart' as validator;

void main() => runApp(Monappli());

class Monappli extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Form Demo'),
        ),
        body: TestForm(),
      ),
    );
  }
}

class TestForm extends StatefulWidget {
  @override
  _TestFormState createState() => _TestFormState();
}

class _TestFormState extends State<TestForm> {
  final _formKey = GlobalKey<FormState>();
  Model model = Model();

  @override
  Widget build(BuildContext context) {
    final halfMediaWidth = MediaQuery.of(context).size.width / 2.0;

    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          MyTextFormField(
            hintText: 'Spécialité',
            validator: (String value) {
              if (value.isEmpty) {
                return 'Selectionner Specialite';
              }
              return null;
            },
            onSaved: (String value) {
              model.lastName = value;
            },
          ),
          MyTextFormField(
            hintText: 'Medecin',
            validator: (String value) {
              if (value.isEmpty) {
                return 'Selectionner un medecin';
              }
              return null;
            },
            onSaved: (String value) {
              model.lastName = value;
            },
          ),
          MyTextFormField(
            hintText: 'Jour',
            validator: (String value) {
              if (value.isEmpty) {
                return 'jour error';
              }
              return null;
            },
            onSaved: (String value) {
              model.lastName = value;
            },
          ),
          MyTextFormField(
            hintText: 'heure',
            validator: (String value) {
              if (value.isEmpty) {
                return 'Heure error';
              }
              return null;
            },
            onSaved: (String value) {
              model.lastName = value;
            },
          ),
          MyTextFormField(
            hintText: 'Motif',
            validator: (String value) {
              if (value.isEmpty) {
                return 'motif error';
              }
              return null;
            },
            onSaved: (String value) {
              model.lastName = value;
            },
          ),


          RaisedButton(
            color: Colors.blueAccent,
            onPressed: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Result(model: this.model)));
              }
            },
            child: Text(
              'Sign Up',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MyTextFormField extends StatelessWidget {
  final String hintText;
  final Function validator;
  final Function onSaved;
  final bool isPassword;
  final bool isEmail;
  final Icon icon;

  MyTextFormField({
    this.hintText,
    this.validator,
    this.onSaved,
    this.isPassword = false,
    this.isEmail = false,
    this.icon
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: TextFormField(
        decoration: InputDecoration(
          hintText: hintText,
          prefixIcon: icon,
          contentPadding: EdgeInsets.all(15.0),
          border: InputBorder.none,
          filled: true,
          fillColor: Colors.grey[200],
        ),
        obscureText: isPassword ? true : false,
        validator: validator,
        onSaved: onSaved,
        keyboardType: isEmail ? TextInputType.emailAddress : TextInputType.text,
      ),
    );
  }
}
class Model {
  String firstName;
  String lastName;
  String email;
  String password;

  Model({this.firstName, this.lastName, this.email, this.password});
}

class Result extends StatelessWidget {
  Model model;
  Result({this.model});

  @override
  Widget build(BuildContext context) {
    return (Scaffold(
      appBar: AppBar(title: Text('Successful')),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(model.firstName, style: TextStyle(fontSize: 22)),
            Text(model.lastName, style: TextStyle(fontSize: 22)),
            Text(model.email, style: TextStyle(fontSize: 22)),
            Text(model.password, style: TextStyle(fontSize: 22)),
          ],
        ),
      ),
    ));
  }
}