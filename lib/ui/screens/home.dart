import 'package:flutter/material.dart';
import 'package:chu/global.dart';
import 'package:chu/ui/screens/details.dart';
import 'package:chu/ui/widgets/doctor_container.dart';
import 'login.dart';
import 'listSpecialite.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'CHU OUAGADOUGOU',
            style: TextStyle(
              fontFamily: 'Texturina'
            ),
          ),
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children:  <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.teal,
                ),
                child: Text(
                  'chu ouagadougou',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                      fontFamily: 'Texturina'
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.calendar_today),
                title: Text('rendez-vous'),
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.info),
                title: Text('Contact'),
                onTap:()=>{},
              ),
              ListTile(
                leading: Icon(Icons.bookmark_border),
                title: Text('A-propos'),
              ),
            ],
          ),
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 9),
                  SizedBox(height: 9),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 4,
                    child: GestureDetector(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailsScreen(id: 0),
                        ),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerRight,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5.0),
                              child: Image.network(
                                "https://image.freepik.com/free-vector/doctor-concept-illustration_114360-1269.jpg",
                              ),
                            ),
                          ),
                          Positioned.fill(
                            child: Container(
                              padding: const EdgeInsets.all(11.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                color: MyColors.blue.withOpacity(.3),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Avez vous les symtomes du Covid-19?",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                    ),
                                    color: MyColors.blue,
                                    child: Text(
                                      "Contacter nous",
                                      style: Theme.of(context).textTheme.button,
                                    ),
                                    onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) =>
                                            DetailsScreen(id: 0),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 9),
                  Text("Nos spécialités", style: Theme.of(context).textTheme.title),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 9.0),
                    height: 71,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: List.generate(
                        categories.length,
                        (f) => Container(
                          constraints: BoxConstraints(
                              maxWidth: MediaQuery.of(context).size.width / 2),
                          margin: const EdgeInsets.symmetric(horizontal: 9.0),
                          padding: const EdgeInsets.all(9.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: MyColors.grey),
                            borderRadius: BorderRadius.circular(9.0),
                          ),
                          child: Row(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                  color: categories[f]['color'],
                                  borderRadius: BorderRadius.circular(9.0),
                                ),
                                child: Image.asset("${categories[f]['icon']}"),
                              ),
                              SizedBox(width: 5),
                              Flexible(
                                child: Text("${categories[f]['title']}"),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(9.0),
              decoration: BoxDecoration(
                color: MyColors.grey,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  topRight: Radius.circular(15.0),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "nos medecins",
                        style: Theme.of(context).textTheme.title,
                      ),
                      FlatButton(
                        child: Text("See All"),
                        onPressed: () {},
                      )
                    ],
                  ),
                  ListView.builder(
                    itemCount: doctorInfo.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) => DoctorContainer(id: i),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
