class Specialite{
 final int departmentid;
 final String departmentname;
 final  String description;
 final String status;

  Specialite({this.departmentid, this.departmentname, this.description, this.status});

  factory Specialite.fromJson(Map<String, dynamic> json) {
    return Specialite(
      departmentid: json['departmentid'] as int,
      departmentname: json['departmentname'] as String,
      description: json['description'] as String,
      status: json['status'] as String,
    );
  }


}