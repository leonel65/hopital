import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:chu/ui/screens/listeRdv.dart';
import 'package:chu/ui/screens/contact.dart';
import 'package:chu/ui/screens/pageRdv.dart';
import 'package:chu/screens/home_screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MainPage extends StatefulWidget
{
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
{
  final List<List<double>> charts =
  [
    [0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4],
    [0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4, 0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4,],
    [0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4, 0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4, 0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4]
  ];

  static final List<String> chartDropdownItems = [ 'Last 7 days', 'Last month', 'Last year' ];
  String actualDropdown = chartDropdownItems[0];
  int actualChart = 0;

  @override
  Widget build(BuildContext context)
  {
    return Scaffold
      (
        appBar: AppBar
          (
          actions: [
            IconButton(
                icon: Icon(Icons.exit_to_app,size: 20,),
                onPressed: ()=> Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => HomeScreen(),
                    )
                )
                ),
          ],
          elevation: 2.0,
          backgroundColor: Colors.blue,
          leading: Padding(padding: EdgeInsets.all(15),
            child: FaIcon(FontAwesomeIcons.userFriends,color: Colors.black38,),
          ),
          centerTitle: true,
          title: Text('Patient', style: TextStyle(color: Colors.black, fontSize: 25.0,fontFamily: 'Texturina')),
        ),
        body: Center(
          child: Padding(
            padding: EdgeInsets.only(top: 20),
            child: StaggeredGridView.count(
              crossAxisCount: 2,
              crossAxisSpacing: 12.0,
              mainAxisSpacing: 12.0,
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              children: <Widget>[
                _buildTile(
                  Padding
                    (
                    padding: const EdgeInsets.all(24.0),
                    child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                            (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              Text('Dossier médical', style: TextStyle(color: Colors.black,fontWeight: FontWeight.w700,fontSize: 22,fontFamily: 'Texturina')),
                            ],
                          ),
                          Material
                            (
                              color: Colors.blue[400],
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center
                                (
                                  child: Padding
                                    (
                                    padding: const EdgeInsets.all(16.0),
                                    child: Icon(Icons.folder_rounded, color: Colors.white, size: 30.0),
                                  )
                              )
                          )
                        ]
                    ),
                  ),
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => ListeRdv())),
                ),
                _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Column
                      (
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>
                        [
                          Padding(
                            padding: EdgeInsets.only(left: 25),
                            child:  Material
                              (

                                color: Colors.teal,
                                shape: CircleBorder(),
                                child: Padding
                                  (
                                  padding: const EdgeInsets.all(15.0),
                                  child: Icon(Icons.assignment, color: Colors.white, size: 25.0),
                                )
                            ) ,
                          ),

                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('Mes rendez-vous',textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 19.0,fontFamily: 'Texturina')),
                        ]
                    ),
                  ),
                ),
                _buildTile(
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: Column
                      (
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>
                        [
                          Padding(
                            padding: EdgeInsets.only(left: 25),
                            child:  Material
                              (

                                color: Colors.redAccent,
                                shape: CircleBorder(),
                                child: Padding
                                  (
                                  padding: const EdgeInsets.all(15.0),
                                  child: Icon(Icons.call, color: Colors.white, size: 25.0),
                                )
                            ) ,
                          ),

                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text('Contact medecin',textAlign: TextAlign.center, style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 19.0,fontFamily: 'Texturina')),
                        ]
                    ),
                  ),
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => Contact())),
                ),
                _buildTile(
                  Padding
                    (
                    padding: const EdgeInsets.all(24.0),
                    child: Row
                      (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>
                        [
                          Column
                            (
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              Text('Prendre RDV', style: TextStyle(color: Colors.black,fontWeight: FontWeight.w700,fontSize: 22,fontFamily: 'Texturina')),
                            ],
                          ),
                          Material
                            (
                              color: Colors.blue[400],
                              borderRadius: BorderRadius.circular(24.0),
                              child: Center
                                (
                                  child: Padding
                                    (
                                    padding: const EdgeInsets.all(16.0),
                                    child: Icon(Icons.lock_clock, color: Colors.white, size: 30.0),
                                  )
                              )
                          )
                        ]
                    ),
                  ),
                  onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => Monappli())),

                ),

              ],
              staggeredTiles: [
                StaggeredTile.extent(2, 110.0),
                StaggeredTile.extent(1, 180.0),
                StaggeredTile.extent(1, 180.0),
                StaggeredTile.extent(2, 110.0),
              ],
            ),
          ),
        )
    );
  }

  Widget _buildTile(Widget child,{Function() onTap}) {
    return Material(
        elevation: 14.0,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0x8021A6FF),
        child: InkWell
          (
          // Do onTap() if it isn't null, otherwise do print()
            onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
            child: child
        )
    );
  }
}