import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:chu/global.dart';
import 'package:chu/ui/screens/details.dart';
import 'package:chu/ui/widgets/doctor_container.dart';
import 'package:chu/ui/screens/login.dart';
import 'package:chu/ui/screens/loginAdmin.dart';
import 'package:http/http.dart' as http;
void main()=>runApp(HomeScreen());

class Constants {
  static const String FirstItem = 'Medecin';
  static const String SecondItem = 'Patient';

  static const List<String> choices = <String>[
    FirstItem,
    SecondItem,
  ];
}

class HomeScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
    throw UnimplementedError();
  }

}

class Home extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _Home();
    throw UnimplementedError();
  }
}

class _Home extends State<Home>{
  void choiceAction(String choice) {
  if (choice == Constants.FirstItem) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => LoginAdmin(),
        )
    );
  } else if (choice == Constants.SecondItem) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => LoginScreen(),
        )
    );  }
  }
  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'CHU OUAGADOUGOU',
            style: TextStyle(
                fontFamily: 'Texturina'
            ),
          ),
          actions: [
            Padding(padding: EdgeInsets.only(top: 400)),
            PopupMenuButton<String>(
              icon: Icon(Icons.assignment_ind),
              elevation: 10.0,
              shape:Border(bottom: BorderSide(width: 2.0,color: Colors.pink[400])),
              onSelected: choiceAction,
              itemBuilder: (BuildContext context) {
                return Constants.choices.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            ),
          ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children:  <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: Colors.blueAccent,
                ),
                child: Text(
                  'chu ouagadougou',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontFamily: 'Texturina'
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.calendar_today),
                title: Text('rendez-vous',style: TextStyle(fontFamily: 'Texturina',fontSize: 17),),
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.info),
                title: Text('Contact',style: TextStyle(fontFamily: 'Texturina',fontSize: 17)),
                onTap:()=>{},
              ),
              ListTile(
                leading: Icon(Icons.bookmark_border),
                title: Text('A-propos',style: TextStyle(fontFamily: 'Texturina',fontSize: 17)),
              ),
            ],
          ),
        ),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 9),
                  SizedBox(height: 9),
                  SizedBox(
                    height: MediaQuery.of(context).size.height / 4,
                    child: GestureDetector(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailsScreen(id: 0),
                        ),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerRight,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(5.0),
                              child: Image.asset('assets/images/coronavirus.png'),
                            ),
                          ),
                          Positioned.fill(
                            child: Container(
                              padding: const EdgeInsets.all(11.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                color: MyColors.blue.withOpacity(.3),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "Avez vous les symtomes du Covid-19?",
                                    style:
                                    TextStyle(fontWeight: FontWeight.bold,fontSize: 18),
                                  ),
                                  Padding(padding: EdgeInsets.only(bottom: 25)),
                                  RaisedButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                    ),
                                    color: MyColors.red,
                                    child: Text(
                                      "Contactez le 0101",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 15
                                      ),
                                    ),
                                    onPressed: () => Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context)=>null,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 9),
                  Text("Nos spécialités", style: TextStyle(fontFamily: 'Texturina',fontSize: 20)),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 9.0),
                    height: 71,
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: List.generate(
                        categories.length,
                            (f) => Container(
                          constraints: BoxConstraints(
                              maxWidth: MediaQuery.of(context).size.width / 2),
                          margin: const EdgeInsets.symmetric(horizontal: 9.0),
                          padding: const EdgeInsets.all(9.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: MyColors.grey),
                            borderRadius: BorderRadius.circular(9.0),
                          ),
                          child: Row(
                            children: <Widget>[
                              Container(
                                padding: const EdgeInsets.all(5.0),
                                decoration: BoxDecoration(
                                  color: categories[f]['color'],
                                  borderRadius: BorderRadius.circular(9.0),
                                ),
                                child: Image.asset("${categories[f]['icon']}"),
                              ),
                              SizedBox(width: 5),
                              Flexible(
                                child: Text("${categories[f]['title']}",style: TextStyle(fontSize: 17),),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(9.0),
              decoration: BoxDecoration(
                color: MyColors.grey,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15.0),
                  topRight: Radius.circular(15.0),
                ),
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                          "Nos medecins",
                          style: TextStyle(fontFamily: 'Texturina',fontSize: 20)),
                    ],
                  ),
                  ListView.builder(
                    itemCount: doctorInfo.length,
                    shrinkWrap: true,
                    itemBuilder: (context, i) => DoctorContainer(id: i),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
