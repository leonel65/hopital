import 'package:flutter/material.dart';

class OnBoardingInstructions {
  final String image, title, subtitle;

  OnBoardingInstructions(this.image, this.title, this.subtitle);
}

class MyColors {
  static const grey = Color(0xfff3f3f3),
      orange = Color(0xffffb755),
      red = Color(0xffed5568),
      lightGreen = Color(0xffdbf3e8),
      darkGreen = Color(0xff4ac18e),
      blue = Color(0xff40beee);
}

class DoctorInfo {
  final String name,
      image,
      type,
      reviewCount,
      about,
      workingHours,
      patientsCount,
      experience,
      certifications;
  final double reviews;

  DoctorInfo({
    this.name,
    this.image,
    this.type,
    this.reviews,
    this.reviewCount,
    this.about,
    this.workingHours,
    this.patientsCount,
    this.experience,
    this.certifications,
  });
}

List<DoctorInfo> doctorInfo = [
  DoctorInfo(
    image:
        "assets/images/doctor.png",
    about:
        "Incidunt placeat eos magni quas quam in dignissimos. Asperiores porro distinctio nemo excepturi labore?",
    certifications: "10",
    experience: "15",
    name: "Dr alex",
    patientsCount: "385",
    reviewCount: "115",
    reviews: 4.3,
    type: "Generaliste",
    workingHours: "lun - vend 09:00 - 17:00",
  ),
  DoctorInfo(
    image:
    "assets/images/d.png",
    about:
        "Incidunt placeat eos magni quas quam in dignissimos. Asperiores porro distinctio nemo excepturi labore?",
    certifications: "10",
    experience: "15",
    name: "Dr gaby",
    patientsCount: "385",
    reviewCount: "98",
    reviews: 4.3,
    type: "Dentiste",
    workingHours: "lun - vend 09:00 - 17:00",
  ),
  DoctorInfo(
    image:
    "assets/images/doctor1.png",
    about:
        "Incidunt placeat eos magni quas quam in dignissimos. Asperiores porro distinctio nemo excepturi labore?",
    certifications: "10",
    experience: "15",
    name: "Dr Joel",
    patientsCount: "385",
    reviewCount: "120",
    reviews: 4.3,
    type: "General Practitioner",
    workingHours: "lun - vend 09:00 - 17:00",
  ),
];

List<OnBoardingInstructions> onBoardingInstructions = [
  OnBoardingInstructions(
    "assets/images/onboarding2.png",
    "Call a doctor to visit you",
    "Incidunt placeat eos magni quas quam in dignissimos. Asperiores porro distinctio nemo excepturi labore?",
  ),
  OnBoardingInstructions(
    "assets/images/doctor1.png",
    "Call a doctor to visit you",
    "Incidunt placeat eos magni quas quam in dignissimos. Asperiores porro distinctio nemo excepturi labore?",
  ),
  OnBoardingInstructions(
    "assets/images/doctor3.png",
    "Call a doctor to visit you",
    "Incidunt placeat eos magni quas quam in dignissimos. Asperiores porro distinctio nemo excepturi labore?",
  ),
  OnBoardingInstructions(
    "assets/images/doctor2.png",
    "Call a doctor to visit you",
    "Incidunt placeat eos magni quas quam in dignissimos. Asperiores porro distinctio nemo excepturi labore?",
  ),
];

List<Map<String, dynamic>> categories = [
  {
    'icon': 'assets/icons/doctor.png',
    'title': 'Generaliste',
    'color': MyColors.red,
  },
  {
    'icon': 'assets/icons/tooth.png',
    'title': 'Dentist',
    'color': MyColors.orange,
  },
  {
    'icon': 'assets/icons/eye_specialist.png',
    'title': 'ophtalmologue',
    'color': MyColors.darkGreen,
  },
];

String avatar =
    "https://pbs.twimg.com/profile_images/1233356631188082689/qcmTohZh_400x400.jpg";
